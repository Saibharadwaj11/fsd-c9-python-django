from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def say_hello(request):
    #use the request.GET dictionary
    name = request.GET.get('name', 'World')
    num_beers = int(request.GET.get('beers', 100)) - 1
    context = {'name': name, 'num_beers': num_beers}
    return render(request, 'fibonacci/hello.html', context)

def nth_fibonacci(n: int) -> int:
    if n <= 1:
        return 0
    if n == 2:
        return 1
    return nth_fibonacci(n - 1) + nth_fibonacci(n - 2)

def thirtieth_fibonacci(request):
    return HttpResponse(nth_fibonacci(30))

def fibonacci_series (request, limit):
    series = [nth_fibonacci(n) for n in range(limit)]
    return HttpResponse(f'''
    <!DOCTYPE html>
    <html>
        <body>
            <h1>Fibonacci Series</h1>
            <p>{series}</p>
        </body>
    </html>
    ''')