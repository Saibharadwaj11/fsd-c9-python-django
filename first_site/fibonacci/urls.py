from django.urls import path
from . import views
urlpatterns = [
    path('hello', views.say_hello, name='hello'),
    path('fib30', views.thirtieth_fibonacci, name='thirtieth_fibonacci'),
    path('series/<int:limit>', views.fibonacci_series, name='fibonacci_series'),
]